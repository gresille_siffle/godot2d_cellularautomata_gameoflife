extends Control

signal add_life
signal add_death

var resolution = 0

func _ready():
    self.resolution = get_node('/root/Game').resolution

func _input(event):
    var tree = get_tree()

    if event.is_action_pressed('game_pause'):
        self.pause(tree, not tree.paused)

    elif event.is_action_pressed('game_restart'):
        self.pause(tree, false)
        get_parent().restart()
    elif event.is_action_pressed('game_wipe'):
        self.pause(tree, false)
        get_parent().restart(true)

    elif event.is_action_pressed('sim_add_life'):
        var mouse = self.get_viewport().get_mouse_position()
        self.add_life(Vector2(int(mouse.x / resolution), int(mouse.y / resolution)))
    elif event.is_action_pressed('sim_add_death'):
        var mouse = self.get_viewport().get_mouse_position()
        self.add_death(Vector2(int(mouse.x / resolution), int(mouse.y / resolution)))

func pause(tree, paused):
    tree.paused = paused

func add_life(cell):
    emit_signal('add_life', cell)

func add_death(cell):
    emit_signal('add_death', cell)
