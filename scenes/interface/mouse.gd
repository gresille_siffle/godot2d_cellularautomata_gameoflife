extends Node2D

const COLOR_WHITE = Color('#ffffff')

var birth = []
var death = []
var resolution = 0

func _ready():
    self.resolution = get_node('/root/Game').resolution

func _physics_process(delta):
    self.update()

    # draw a living cell
    if Input.is_action_pressed('sim_add_life'):
        var mouse = get_viewport().get_mouse_position()
        var cell = Vector2(int(mouse.x / resolution), int(mouse.y / resolution))
        if not cell in birth:
            birth.append(cell)
            get_parent().add_life(cell)

    elif Input.is_action_just_released('sim_add_life'):
        birth.clear()

    # draw a dead cell
    elif Input.is_action_pressed('sim_add_death'):
        var mouse = get_viewport().get_mouse_position()
        var cell = Vector2(int(mouse.x / resolution), int(mouse.y / resolution))
        if not cell in death:
            birth.append(cell)
            get_parent().add_death(cell)

    elif Input.is_action_just_released('sim_add_death'):
        death.clear()

func _draw():
    var mouse = self.get_viewport().get_mouse_position()
    var focused_cell_x = int(mouse.x / resolution) * resolution
    var focused_cell_y = int(mouse.y / resolution) * resolution

    var top_left = Vector2(
        focused_cell_x,
        focused_cell_y - int(resolution / 2)
    )
    var top_rght = Vector2(
        focused_cell_x + resolution,
        focused_cell_y - int(resolution / 2)
    )

    var left_top = Vector2(
        focused_cell_x - int(resolution / 2),
        focused_cell_y
    )
    var left_dwn = Vector2(
        focused_cell_x - int(resolution / 2),
        focused_cell_y + resolution
    )

    var add_x = Vector2(resolution * 2, 0)
    var add_y = Vector2(0, resolution * 2)

    draw_line(left_top, left_top + add_x, COLOR_WHITE)
    draw_line(left_dwn, left_dwn + add_x, COLOR_WHITE)
    draw_line(top_left, top_left + add_y, COLOR_WHITE)
    draw_line(top_rght, top_rght + add_y, COLOR_WHITE)
