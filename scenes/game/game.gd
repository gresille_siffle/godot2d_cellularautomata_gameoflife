extends Node

var resolution = 12

func _ready():
    $Interface.connect('add_life', self, '_on_Interface_add_life')
    $Interface.connect('add_death', self, '_on_Interface_add_death')
    self.restart()

func restart(clear=false):
    $Board.restart(resolution, clear)

func _on_Interface_add_life(cell):
    $Board.add_life(cell)

func _on_Interface_add_death(cell):
    $Board.add_death(cell)
