extends Node2D

const COLOR_BLACK = Color('#000000')
const COLOR_WHITE = Color('#ffffff')

var rows = 0
var cols = 0
var grid = []
var resolution = null

func _ready():
    $Timer.connect('timeout', self, '_on_Timer_timeout')

func _draw():
    for row in range(self.rows):
        for col in range(self.cols):
            if self.grid[row][col] == 1:
                draw_rect(Rect2(col * resolution, row * resolution, resolution, resolution), COLOR_WHITE)
            else:
                draw_rect(Rect2(col * resolution, row * resolution, resolution, resolution), COLOR_BLACK)

func step():
    self._new_generation()
    self.update()

func restart(resolution=12, clear=false):
    if not $Timer.is_stopped():
        $Timer.stop()

    self.resolution = resolution
    var screen_size = get_viewport_rect().size
    self.cols = int(screen_size.x / self.resolution)
    self.rows = int(screen_size.y / self.resolution)
    self.grid.clear()

    for y in range(self.rows):
        var row = []
        for x in self.cols:
            if clear:
                row.append(0)
            else:
                randomize()
                row.append(randi() % 2)
        self.grid.append(row)

    $Timer.start()

func _new_generation():
    var next = []

    for row in range(self.rows):
        next.append([])
        for cell in range(self.cols):
            next[row].append(self._future_cell_state(cell, row, self.grid[row][cell]))

    self.grid = next

func _future_cell_state(x, y, current_state):
    """Apply the rules of life to the cell at the (x, y) coordinates, considering its current state."""
    var neighbors = 0;

    var l_neighbor = x - 1
    var r_neighbor = x + 1
    var t_neighbor = y - 1
    var b_neighbor = y + 1

    if x == 0:
        l_neighbor = self.cols - 1
    elif x == self.cols - 1:
        r_neighbor = 0

    if y == 0:
        t_neighbor = self.rows - 1
    elif y == self.rows - 1:
        b_neighbor = 0

    var top_row = grid[t_neighbor]
    var cur_row = grid[y]
    var bot_row = grid[b_neighbor]

    # top row
    if top_row[l_neighbor] == 1:
        neighbors += 1
    if top_row[x] == 1:
        neighbors +=1
    if top_row[r_neighbor] == 1:
        neighbors += 1

    # middle row
    if cur_row[l_neighbor] == 1:
        neighbors += 1
    if cur_row[r_neighbor] == 1:
        neighbors += 1

    # bottom row
    if bot_row[l_neighbor] == 1:
        neighbors += 1
    if bot_row[x] == 1:
        neighbors += 1
    if bot_row[r_neighbor] == 1:
        neighbors += 1

    # if the cell is alive, it can die from loneliness
    if current_state == 1 and neighbors < 2:
        return 0

    # ... or it can die from overpopulation
    if current_state == 1 and  neighbors > 3:
        return 0

    # if the cell is dead, it can rebirth with exactly 3 neihbors
    elif current_state == 0 and neighbors == 3:
        return 1

    # ... otherwise, it remains in the same state
    else:
        return current_state

func add_life(cell):
    self.grid[cell.y][cell.x] = 1
    self.update()

func add_death(cell):
    self.grid[cell.y][cell.x] = 0
    self.update()

func _on_Timer_timeout():
    self.step()
